class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :post_id
      t.integer :parent
      t.string :author
      t.text :message

      t.timestamps
    end
  end
end
