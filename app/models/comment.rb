class Comment < ActiveRecord::Base
	belongs_to 		:posts
	validates :author, :message, presence: true
end
